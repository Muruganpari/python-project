#!/bin/python3

import math
import os
import random
import re
import sys
from datetime import datetime

def countingValleys(steps, path):
    # Write your code here
    level = 0
    count = 0
    
    for i in path:
        if i == 'U':
            level += 1
        elif i == 'D':
            level -= 1
        if level == 0:
            if prev < level:
                count += 1
        prev = level
        
    return count

if __name__ == '__main__':

    steps = int(input().strip())

    path = input()

    start_time = datetime.now()
    result = countingValleys(steps, path)
    diff = datetime.now() - start_time
    print("The time of execution of above program is :" , (str(diff)[5:]))

    print(str(result) + '\n')


